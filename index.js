let numInputIsTrue = false;
let number;

while (numInputIsTrue === false) {
  number = Number(prompt("Enter a number:"));
  // console.log(number);
  if (number !== NaN && number > 50) {
    console.log("The number you provided is " + number + ".");
    numInputIsTrue = true;
  } else if (isNaN(number)) {
    alert("Enter a number and must be greater than 50");
  } else if (number == 0) {
    break;
  }
}

for (let iter = number; iter >= 50; iter--) {
  if (iter == 50) {
    console.log("The current value is 50. Terminating the loop.");
  } else if (iter % 10 === 0) {
    console.log("The " + iter + " is divisible by 10. Skipping the number.");
    continue;
  } else if (iter % 5 === 0) {
    console.log(iter);
  }
}

let word = "supercalifragilisticexpialidocious";
let vowel = ["a", "e", "i", "o", "u"];
let isVowel,
  consonantsOnly = "";
for (let counter = 0; counter < word.length; counter++) {
  // console.log(word[counter]);
  isVowel = vowel.includes(word[counter]);

  if (isVowel === true) {
    continue;
  } else {
    consonantsOnly += word[counter];
  }
}

console.log(word);
console.log(consonantsOnly);
